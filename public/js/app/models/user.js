define(
    ['jquery','backbone'],
    function($,Backbone){
    
    var user = Backbone.Model.extend({
        'default': {
            uid:'',
            lName:'',
            fName:'',
            mName:'',
            price:'',
            date:'',
            photo:'',
            about:''
        },
        initialize: function(){
            this.set('prefix_price',this.formatPrice(this.get('price')));
            this.set('prefix_date',this.formatDate(this.get('date')));
            this.set('filter','true'); // для фильтрации
            this.set('page','false'); // для пагинации
        },
        
        formatDate: function(value){
            // ie8
            var value = value.replace(/(\d+)-(\d+)-(\d+)/, '$2/$3/$1');
            return new Date(value).toLocaleDateString("ru",{
                    year: 'numeric',
                    month: 'long',
                    day: 'numeric',
                }).split('г.')[0];
        },
        
        formatPrice: function(value){
            //console.log(value)
            return (value).toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
        }
    });
    
    return user;
    
});