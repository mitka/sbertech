define(
    ['jquery',
    'underscore',
    'backbone',
    'collections/usersCollection'],
    
    function($,_,Backbone,usersCollection){

        return Backbone.View.extend({
            el: $('.toolbar .filter'),
            
            initialize: function(){
                this.render();
            },
            events: {
                "keyup": "filterKeyUp",
            },
            /*
                Один символ перебрасывается в следующую ячейку.
            */
            formationArray: function(array){
                var a = [],
                    first='';
                _.each(array,function(val){
                    if (val.toString().split('').length>1 || !!first){
                        a.push($.trim(first+' '+val))
                        first = '';
                    }
                    else{
                        first = val;
                    }
                });
                return a;
            },
            
            filterKeyUp: function(){
                var inputValue = this.$el.val(), 
                    regexp='',
                    _self = this;
                
                
                if(inputValue.length > 3){
                    /*
                        Формируем regexp
                    */
                    var output = $.trim(inputValue.toLowerCase()).split(' ');
                    regexp = new RegExp(this.formationArray(output).join('|'), 'g');
                    
                }
                else regexp = new RegExp('','g');
                
                
                var users = usersCollection.each(function(item) {

                    
                    var arrayTmpl = 0;
                    //if(item.get('filter') == 'true' || inputValue.length <= 3){
                        var tmpl = (item.get('uid')+ ' ' + item.get('lName')+ ' '+ item.get('fName')+ ' '+ item.get('mName')+ ' '+ item.get('price')+ ' ' + item.get('prefix_date')).toLowerCase();
                        arrayTmpl = tmpl.match(regexp);
                        arrayTmpl = arrayTmpl != null ? arrayTmpl.length : arrayTmpl;
                    //}   
                    
                        if(arrayTmpl >= _self.formationArray($.trim(inputValue).split(' ')).length){
                            item.set('filter','true');
                            return item;
                        }
                        else{
                            item.set('filter','false');
                            return item;
                        }
                   
                });
                
                usersCollection.reset(users);
                 // Вызов эвента для обновлении пагинации
                usersCollection.trigger('resetFilter');
                    
            }
            
            
        });
        
    
    });