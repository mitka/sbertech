define(
    ['jquery',
    'underscore',
    'backbone'],
    
    function($,_,Backbone){

        return Backbone.View.extend({
            el: $('#information'),
            
            template: _.template($('#info-user').html()),
            initialize: function(){
                this.render();
            },
            render: function() {
                this.$el.html(this.template(this.model.toJSON()));
                return this;	
            },
            
            
        });
        
    
    });