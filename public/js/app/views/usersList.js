define(
    ['jquery',
    'underscore',
    'backbone',
    'views/userItem',
    ],
    
    function($,_,Backbone,userItem){
    
    
    
        var userList = Backbone.View.extend({
            
            el: $('.users'),
            
            initialize: function(){
                this.collection.on('loadUserCollection', this.render, this);
                this.collection.on('sort', this.render, this);
                this.collection.on('reset', this.render, this);
            
            },
            
            render: function() {
                this.$el.empty();
                this.collection.each(this.item, this);
			    return this;
            },
            
            item: function(model) {
                var item = new userItem({ model: model });
                this.$el.append( item.render().el );
            },
            
            
        });
    
    return userList;

    
});