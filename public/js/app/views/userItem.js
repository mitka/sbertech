define(
    ['jquery',
    'underscore',
    'backbone',
    'views/infoItem'],
    
    function($,_,Backbone,infoItem){

        return Backbone.View.extend({
            tagName: 'li',
            template: _.template($('#item-users').html()),
            render: function() {
                this.$el.html(this.template(this.model.toJSON() ));
                return this;	
            },
            
            events: {
                "click": "openInfo",
            },
            
            openInfo: function(){
                this.$el.parent().find('li').removeClass('active');
                this.$el.addClass('active');
                new infoItem({model:this.model});
            }
            
        });
        
    
    });