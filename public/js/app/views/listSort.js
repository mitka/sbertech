define(
    ['jquery',
    'underscore',
    'backbone',
    'collections/usersCollection',
    ],
    
    function($,_,Backbone,usersCollection){
    
        var listOl = Backbone.View.extend({
            
            el: $('.list ol li'),
            
            initialize: function(){},
            
            events: {
                "click": "sorting",
            },
            
            sorting: function(e){
                var _this = $(e.currentTarget);
                
                /*
                    Сортируем по столбцу _this.data('col')
                */
                usersCollection.sorting(_this.data('col'));
                
                this.$el.removeClass('active');
                _this.addClass('active');
            }
            
            
        });
        return listOl;
});