define(
    ['jquery',
    'underscore',
    'backbone',
    'collections/usersCollection'],
    
    function($,_,Backbone,usersCollection){

        return Backbone.View.extend({
            el: $('.toolbar .pagination'),
            
            showItem: 10,   // Отображать по
            countPage: 0,   // Всего страниц
            activePage: 1,  // Активная страница
            
            templatePage: _.template('<li><a href="#" class="<%- c %>"><%- i %></a></li>'), 
            
            initialize: function(){
                usersCollection.on('loadUserCollection',  this.render, this);
                usersCollection.on('sort',  this.render, this);
                usersCollection.on('resetFilter', function(){
                    this.activePage = 1; //сбрасываем на первую страницу
                    this.render();
                }, this);
                this.render();
            },
            events: {
                "click .page a": "clickPage",
                "change select": "changeSelect",
            },
            /*
                Изменения значения в селекте
            */
            changeSelect: function(){
                this.activePage = 1;
                this.render();
            },
            /*
                Клик по пагинации
            */
            clickPage: function(e){
                var _this = $(e.currentTarget);
                
                this.$el.find(".page a").removeClass('active');
                _this.addClass('active');
                
                this.activePage = _this.text();
                this.render();
            },
            
            render: function(){
                this.showItem = this.$el.find('select').val();
                var countCollection = usersCollection.where({filter:'true'}).length;
                this.countPage = Math.floor(countCollection/this.showItem);
                
                
                var pageList = this.$el.find('.page');
                pageList.find('li').remove();
                
                for(var i=1; i<this.countPage+2; i++){
                    
                    pageList.append(this.templatePage({
                        i:i,
                        c: this.activePage == i ? "active" : ""
                    }));
                }
                
                this.page();
            },
            /*
                Выборка и обновление коллекции
            */
            page: function(page){
                
                this.activePage = page || this.activePage;
                
                var first   = (this.activePage*this.showItem)-this.showItem,
                    last    = this.activePage*this.showItem;
                
                
                // Находим только отфильтрованных
                var users = usersCollection.where({'filter':'true'});
                var pageActive = users.slice(first,last);
                // ie8
                pageActive = _.map(pageActive,function(val){return val.cid;});

                var users = usersCollection.forEach(function(item){
                    // Сопоставляем отфильтрованных с полной коллекцией
                    // Page == true только у тех у кого совпал cid с с фильтрованным cid
                    if(!!_.intersection(pageActive, [item.cid]).length){
                        item.set('page','true')
                    }
                    else item.set('page','false')
                });
                
                
                usersCollection.reset(users);
                
            }
            
            
        });
        
    
    });