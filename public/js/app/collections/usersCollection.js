define(
    ['jquery','backbone',
    'models/user',
    ],
    function($,Backbone,userModel){
    
    var usersCollection = Backbone.Collection.extend({
        model:userModel,
        url: '/data/data.json',
        
        
        sortCol: 'uid', // По умолчанию сортировать по uid
        sortDirection: 1, // Сортировка от меньшего к большему
        
        
        comparator: function(a,b){
            // аналог .sort()
            var a = a.get(this.sortCol),
                b = b.get(this.sortCol);
            
            if (a == b) return 0;
            
            if (this.sortDirection == 1) {
                return a > b ? 1 : -1;
            } else {
                return a < b ? 1 : -1;
            }
            
        },
        
        
        sorting: function(attr) {
            
            // Направление  сортировки от большего к меньшему и наоборот
            if (this.sortCol == attr) this.sortDirection *= -1;
            else this.sortDirection = 1;
            
            this.sortCol = attr;
            this.sort();   
        }
        
        
        
    });
    
    var users = new usersCollection();
    users.fetch({
        success: function(){
            users.trigger('loadUserCollection');
        },
        error: function(){
            //console.log('Error collection');
        }
    });
    return users;
    
});