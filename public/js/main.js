requirejs.config({
    
    baseUrl: 'js/app',
    
    urlArgs: "bust=" + (new Date()).getTime(),
    
    paths: {
        jquery: '/js/lib/jquery/jquery.min',
        underscore: '/js/lib/underscore/underscore-min',
        backbone: '/js/lib/backbone/backbone'
    },
    deps: ['/js/app.js']
});
