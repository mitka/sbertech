define([
	'jquery',
    'backbone',
    'views/usersList',
    'views/listSort',
    'collections/usersCollection',
    'views/userFilter',
    'views/userPagination',
	],function(
		$,
		Backbone,
		usersList,
		listOl,
		usersCollection,
		userFilter,
		userPagination){
	    
	    
		/*
	    	Пагинация
	    */
		new userPagination();
	    
	    /*
	    	Запускаем сортировку коллекции
	    */
		new listOl();
		
		/*
	    	Запускаем фильтр
	    */
		new userFilter();
		
	    
	    /*
	    	Формируем список пользователей
	    */
        var users = new usersList({
            collection: usersCollection,
        })
        users.render();
	    
	    
		// placeholder ie
		$('[placeholder]').focus(function() {
				var input = $(this);
				if (input.val() == input.attr('placeholder')) {
					input.val('');
					input.removeClass('placeholder');
				}
			}).blur(function() {
				var input = $(this);
				if (input.val() == '' || input.val() == input.attr('placeholder')) {
					input.addClass('placeholder');
					input.val(input.attr('placeholder'));
				}
			}).blur();
	    
});