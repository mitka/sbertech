module.exports = function(grunt) {
    grunt.initConfig({

        less: {
            production:{
                options: {
                    paths: ["public/"],
                    cleancss: true
                },
                files: {
                    "public/css/build.css": "public/less/styles.less"
                }
            }
        },
        
       
    });
       
    grunt.loadNpmTasks('grunt-contrib-less');


    grunt.registerTask('default', ['less']);

};